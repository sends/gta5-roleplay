# Changelog GTA 5 Fivem Roleplay Server

#### Legende :
|Alias      | Définitions                           |
|-----------|---------------------------------------|
|Added      | Ajout d'une nouvelles fonctionnalités |
|Changed    | Changement de fonctionnalités         |
|Deprecated | Fonctionnalité à supprimer            |
|Removed    | Suppression d'une fonctionnalités     |
|Fixed      | Corrections de bugs                   |

## Unreleased
 - ..
 

## [0.0.2] - 2019-08-20
### Added
- Ajout d'un barbier et d'un coiffeur #10
- Ajout d'un menu d'admin simple
- Ajout d'un anti-cheat
- Ajout de la possibilité de verouiller son véhicule #2

### Fixed
- Suppression des drops d'armes sur les NPCs #1
- Suppression des gives d'armes dans les véhicules #1

## [0.0.1] - 2019-08-19
### Added
- Git